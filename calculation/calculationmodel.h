/**************************************************************************
**  File: calculation.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 15.08.2019
**************************************************************************/

#pragma once

#include "calculationmodel_global.h"
#include <QObject>
#include <QPointF>

/**
 * @brief The CalculationModel class класс содержит методы вычислений реализован по шаблону Singleton
 */
class CALCULATIONSHARED_EXPORT CalculationModel: public QObject
{
    Q_OBJECT

public:
    CalculationModel();

    /**
     * @brief calculationSlewPoints метод реализует псевдовычисления (для демонстрации)
     * @param bearing   текущий пеленг
     * @param slew      текущий снос
     * @param distance  радиус (дальность до точки)
     */
    void calculationSlewPoints(double bearing, double slew, double distance);

    /**
     * @brief instance метод извлечения указателя на этот класс
     * @return  new если null
     */
    static CalculationModel *instance();

    /**
     * @brief release освобождение памяти делегировано
     */
    static void release();

signals:
    /**
     * @brief result сигнал передает вычисленную точку
     */
    void result(QPointF);

private:
    ///< указатель на этот клсаа
    static CalculationModel* m_instance;
    /**
     * @brief rad метод перевода градусов в радианы
     * @param x   градусы
     * @return    радианы
     */
    double gradToRad(double x);
};


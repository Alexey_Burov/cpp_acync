/**************************************************************************
**  File: calculation.cpp
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 15.08.2019
**************************************************************************/

#include "calculationmodel.h"
#include <QDebug>
#include <QThread>
#include <qmath.h>

CalculationModel* CalculationModel::m_instance = nullptr;

CalculationModel::CalculationModel()
{
    m_instance = nullptr;
}

void CalculationModel::calculationSlewPoints(double bearing, double slew, double distance)
{
    while (bearing > slew ? bearing > slew : slew  > bearing) {
        double x = distance * qSin(gradToRad(bearing));
        double y = distance * qCos(gradToRad(bearing));
        emit result(QPointF(x,y));
        QThread::msleep(1000);
        bearing > slew ? --bearing: --slew;
    }
}

CalculationModel *CalculationModel::instance()
{
    if (m_instance == nullptr)
    {
        m_instance = new CalculationModel();

    }
    return m_instance;
}

void CalculationModel::release()
{
    if(m_instance) delete m_instance;
}

double CalculationModel::gradToRad(double x) {
    return x * 3.14 / 180.0;
}

#-------------------------------------------------
#
# Project created by QtCreator 2019-08-15T14:32:40
#
#-------------------------------------------------
CONFIG += c++11

QT       -= gui

TARGET = calculation
TEMPLATE = lib

DEFINES += CALCULATION_LIBRARY

SOURCES += \
    calculationmodel.cpp

HEADERS +=\
    calculationmodel.h \
    calculationmodel_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

/**************************************************************************
**  File: calculationmodel_global.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 16.08.2019
**************************************************************************/

#pragma once

#include <QtCore/qglobal.h>

#if defined(TEST_LIBRARY)
#  define CALCULATIONSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CALCULATIONSHARED_EXPORT Q_DECL_IMPORT
#endif

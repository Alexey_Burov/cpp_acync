/**************************************************************************
**  File: meansstatesubclass.cpp
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 16.08.2019
**************************************************************************/

#include "meansstatesubclass.h"

MeansStateSubClass::MeansStateSubClass()
{
}
MeansStateSubClass::Condition MeansStateSubClass::condition() const
{
    return m_condition;
}

void MeansStateSubClass::setCondition(const Condition &c_condition)
{
    m_condition = c_condition;
}

MeansStateSubClass::Conception MeansStateSubClass::conception() const
{
    return m_conception;
}

void MeansStateSubClass::setConception(const Conception &c_conception)
{
    m_conception = c_conception;
}

MeansStateSubClass::TypeWorks MeansStateSubClass::works() const
{
    return m_works;
}

void MeansStateSubClass::setWorks(const TypeWorks &c_works)
{
    m_works = c_works;
}

MeansStateSubClass::WidgetType MeansStateSubClass::type() const
{
    return m_type;
}

void MeansStateSubClass::setType(const WidgetType &c_type)
{
    m_type = c_type;
}





/**************************************************************************
**  File: baseclass.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 15.08.2019
**************************************************************************/

#pragma once

#include <QObject>
#include <QSharedPointer>

#include "calculationmodel.h"
#include "means.h"

/**
 * @brief The BaseClass class базовый класс
 */
class BaseClass : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief BaseClass
     * @param parent
     */
    explicit BaseClass(QObject *parent = 0);

    /**
     * освобождение памяти
     */
    ~BaseClass();

    /**
     * @brief initCalculationSlewPoints инициализация вычислений
     */
    void initCalculationSlewPoints();
signals:

private slots:
    /**
     * @brief handlerCalculation слот обработчик вычислений
     * @param result
     */
    void handlerCalculation(QPointF result);
private:

    ///< smartpointer одного средства
    QSharedPointer<Means> m_means;

    /**
     * @brief initMeans инициализация средств
     */
    void initMeans();

};

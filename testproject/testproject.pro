#-------------------------------------------------
#
# Project created by QtCreator 2019-08-15T08:23:14
#
#-------------------------------------------------
CONFIG += c++11

QT       += core concurrent

QT       -= gui

TARGET = testproject
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app



SOURCES += main.cpp \
    baseclass.cpp \
    meansstatesubclass.cpp \
    meansdrawsubclass.cpp \
    means.cpp
HEADERS += \
    baseclass.h \
    meansstatesubclass.h \
    meansdrawsubclass.h \
    means.h

unix:!macx: LIBS += -L$$OUT_PWD/../calculation/ -lcalculation

INCLUDEPATH += $$PWD/../calculation
DEPENDPATH += $$PWD/../calculation

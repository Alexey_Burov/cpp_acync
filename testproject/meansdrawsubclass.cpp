/**************************************************************************
**  File: meansdrawsubclass.cpp
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 16.08.2019
**************************************************************************/

#include "meansdrawsubclass.h"

MeansDrawSubClass::MeansDrawSubClass()
{
}

QRect MeansDrawSubClass::geometry() const
{
    return m_geometry;
}

void MeansDrawSubClass::setGeometry(const QRect &c_geometry)
{
    m_geometry = c_geometry;
}

QPoint MeansDrawSubClass::widgetPosition() const
{
    return m_widgetPosition;
}

void MeansDrawSubClass::setWidgetPosition(const QPoint &c_widgetPosition)
{
    m_widgetPosition = c_widgetPosition;
}

QPoint MeansDrawSubClass::pickOffset() const
{
    return m_pickOffset;
}

void MeansDrawSubClass::setPickOffset(const QPoint &c_pickOffset)
{
    m_pickOffset = c_pickOffset;
}




/**************************************************************************
**  File: baseclass.cpp
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 15.08.2019
**************************************************************************/

#include "baseclass.h"
#include <QFuture>

#include <QList>
#include <QThread>
#include <QtConcurrent/QtConcurrent>

///< безымянный namespace данные для проверки
namespace {
///< пеленг от первого
const int c_beringFirst = 4;
///< пеленг от второго
const int c_beringSecond = 2;
///< снос от первого
const int c_slewFirst = 2;
///< снос от второго
const int c_slewSecond = 6;
///< радиус (дальность до точки)
const int c_distance = 100;
///< имя средства
const QString c_meansName = "mr-650";
}

BaseClass::BaseClass(QObject *parent) :
    QObject(parent)
{
    connect(CalculationModel::instance(), &CalculationModel::result,this,&BaseClass::handlerCalculation);

    initMeans();
}

BaseClass::~BaseClass()
{
    CalculationModel::release();
}

void BaseClass::initCalculationSlewPoints()
{
    CalculationModel *model = CalculationModel::instance();
    QtConcurrent::run(model, &CalculationModel::calculationSlewPoints, c_beringFirst, c_slewFirst, c_distance);
    QtConcurrent::run(model, &CalculationModel::calculationSlewPoints, c_beringSecond, c_slewSecond, c_distance);
}

void BaseClass::handlerCalculation(QPointF result)
{
    m_means.data()->setDriftage(result);
}

void BaseClass::initMeans()
{
    m_means = QSharedPointer<Means>(new Means(c_meansName));
    m_means.data()->setConception(Means::Conception::Preview);
    m_means.data()->setCondition(Means::Condition::Off);
    m_means.data()->setType(Means::WidgetType::Deff);
    m_means.data()->setWorks(Means::TypeWorks::Work);
}

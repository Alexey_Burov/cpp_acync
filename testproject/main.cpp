/**************************************************************************
**  File: main.cpp
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 15.08.2019
**************************************************************************/

#include <QCoreApplication>
#include "baseclass.h"


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    BaseClass bc;
    bc.initCalculationSlewPoints();

    return a.exec();
}

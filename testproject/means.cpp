/**************************************************************************
**  File: means.cpp
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 16.08.2019
**************************************************************************/

#include "means.h"
#include <QDebug>

Means::Means()
{
}

Means::Means(const QString &c_name)
{
    m_name = c_name;
}

QString Means::name() const
{
    return m_name;
}

void Means::setName(const QString &c_name)
{
    m_name = c_name;
}
QPointF Means::driftage() const
{
    return m_driftage;
}

void Means::setDriftage(const QPointF &c_driftage)
{
    m_driftage = c_driftage;
    qDebug() << "driftage" << m_driftage;
}



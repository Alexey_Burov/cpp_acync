/**************************************************************************
**  File: meansstatesubclass.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 16.08.2019
**************************************************************************/

#pragma once

/**
 * @brief The MeansStateSubClass class вспомогательный класс хранит текущее состояние средства
 */
class MeansStateSubClass
{
public:
    MeansStateSubClass();

    /**
     * @brief The Condition enum состояние
     */
    enum class Condition{
        On,         ///< вкл.
        Off,        ///< выкл.
        Fault,      ///< неисправен
        Reserve     ///< резервный
    };

    /**
     * @brief The Conception enum концепт графического представления
     */
    enum class Conception{
        Preview,    ///< превью
        Widget,     ///< развернутый формуляр
        Refer       ///< свернуто
    };

    /**
     * @brief The TypeWidget enum тип отоброажения виджета
     */
    enum class WidgetType{
        Diagram,        ///< диаграмма
        Table,          ///< таблица
        Taxt,           ///< текст
        Deff            ///< по умолчанию
    };

    /**
     * @brief The TypeWorks enum тип режима работы
     */
    enum class TypeWorks{
        Control,    ///< контроль
        Training,   ///< тренаж
        Work        ///< работа
    };


    /**
     * @brief condition метод извлечения текущего состояния
     * @return  перечисляемый тип состояния
     */
    Condition condition() const;

    /**
     * @brief setCondition метод установки текущего состояния
     * @param c_condition  перечисляемый тип состояния
     */
    void setCondition(const Condition &c_condition);

    /**
     * @brief conception метод извлечения текущего концепта графического представления
     * @return  перечисляемый тип  концепта графического представления
     */
    Conception conception() const;

    /**
     * @brief setConception метод установки текущего концепта графического представления
     * @param c_conception  перечисляемый тип  концепта графического представления
     */
    void setConception(const Conception &c_conception);

    /**
     * @brief works метод извлечения текущего режима работы
     * @return  перечисляемый тип текущего режима работы
     */
    TypeWorks works() const;

    /**
     * @brief setWorks метод установки текущего режима работы
     * @param c_works  перечисляемый тип текущего режима работы
     */
    void setWorks(const TypeWorks &c_works);

    /**
     * @brief type метод извлечения текущего типа отоброажения виджета
     * @return  перечисляемый тип текущего режима работы
     */
    WidgetType type() const;

    /**
     * @brief setType метод установки текущего типа отоброажения виджета
     * @param c_type тип текущего режима работы
     */
    void setType(const WidgetType &c_type);

private:
    ///состояние
    Condition m_condition;
    ///концепт графического представления
    Conception m_conception;
    ///тип режима работы
    TypeWorks m_works;
    ///тип отоброажения виджета
    WidgetType m_type;
};

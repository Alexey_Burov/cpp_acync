/**************************************************************************
**  File: meansdrawsubclass.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 16.08.2019
**************************************************************************/

#pragma once

#include <QRect>

/**
 * @brief The MeansDrawSubClass class вспомогательный класс содержит методы для работы с отрисовкой
 */
class MeansDrawSubClass
{
public:
    MeansDrawSubClass();

    /**
     * @brief geometry метод извлечения области отрисовки
     * @return прямоугольник
     */
    QRect geometry() const;

    /**
     * @brief setGeometry метод установки области отрисовки
     * @param c_geometry  прямоугольник
     */
    void setGeometry(const QRect &c_geometry);

    /**
     * @brief widgetPosition метод извлечения местоположения
     * @return точка 2-х коорд.
     */
    QPoint widgetPosition() const;

    /**
     * @brief setWidgetPosition метод установки местоположения
     * @param c_widgetPosition точка 2-х коорд.
     */
    void setWidgetPosition(const QPoint &c_widgetPosition);

    /**
     * @brief pickOffset метод звлечения смещения при сколе
     * @return точка 2-х коорд.
     */
    QPoint pickOffset() const;

    /**
     * @brief setPickOffset метод установки смещения при сколе
     * @param c_pickOffset точка 2-х коорд.
     */
    void setPickOffset(const QPoint &c_pickOffset);

private:
    ///область отрисовки
    QRect m_geometry;
    ///местоположение
    QPoint m_widgetPosition;
    ///смещение при сколе
    QPoint m_pickOffset;
};

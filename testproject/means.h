/**************************************************************************
**  File: means.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 16.08.2019
**************************************************************************/

#pragma once

#include <QObject>

#include "meansdrawsubclass.h"
#include "meansstatesubclass.h"

/**
 * @brief The Means class класс описывает средство наследуется от подклассов отрисовки и состояния
 */
class Means : public QObject, public MeansDrawSubClass, public MeansStateSubClass
{
    Q_OBJECT
public:
    Means();

    /**
     * @brief Means конструктор с установкой имени средства
     * @param c_name
     */
    Means(const QString &c_name);

    /**
     * @brief name метод извлечения имени средства
     * @return строковое значение m_name
     */
    QString name() const;

    /**
     * @brief setName метод установки имени средства
     * @param c_name  строковая константа
     */
    void setName(const QString &c_name);

    /**
     * @brief driftage метод извлечения текущего сноса
     * @return  точка из 2-х координат m_driftage
     */
    QPointF driftage() const;

    /**
     * @brief setDriftage метод установки текущего сноса
     * @param c_driftage константное значение точки из 2-х координат
     */
    void setDriftage(const QPointF &c_driftage);

private:
    ///< имя
    QString m_name;
    ///< снос
    QPointF m_driftage;

};
